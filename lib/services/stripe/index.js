const config = require("config")
const stripe = require("stripe")(config.stripe.secret)

const myTestCard = {
  number: "4242424242424242",
  exp_month: "09",
  exp_year: "23",
  name: "Mihai Perju",
  cvc: "322"
}

const customerId = "cus_JWkbr9V1k1ugOS"
const cardToken = "tok_1Iwth6LjbeBKS6QKFYbWC4zm"
const cardId = "card_1Iwth5LjbeBKS6QKNBLW7N8e"

const demo = async () => {
  // const customers = await getCustomer(customerId)
  // console.log(customers)

  const cardToken = await createCardToken(myTestCard)
  console.log(cardToken)
  // const card = await createCard(customerId, cardToken)
  // console.log(card)
  

  const charge = await createCharge(cardToken.id)
  console.log(charge)
  // console.log({balance})
}

const getBalance = async () => await stripe.balance.retrieve()

const getCustomers = async (limit=1)=> await stripe.customers.list({ limit });

const getCustomer = async (customerId) => await stripe.customers.retrieve(customerId)

const customerCreate = async () => await stripe.customers.create({
  description: 'My First Test Customer (created for API docs)',
})

const createCard = async(customerId, token) => await stripe.customers.createSource(
  customerId,
  {
    source: token
  },
)

const createCardToken = async(card)=>await stripe.tokens.create({card});

const createCharge = async (cardId) =>await stripe.charges.create({
  amount: 10000,
  currency: 'MDL',
  source: cardId,
  description: 'My First Test Charge (created for API docs)',
});

module.exports = {
  demo,
  getBalance,
  stripe,
  customerCreate,
}
