module.exports = {
  smtp: {
    host: "smtp.mailtrap.io",
    port: 2525,
    auth: {
      user: "3499664ff72ccd",
      pass: "90e8874117b025",
    },
  },
  mongo: {
    uri: process.env.MONGO_URI,
    options: {
      useNewUrlParser: true,
      poolSize: 10, // Maintain up to 10 socket connections
      useUnifiedTopology: true,
    },
  },
  port: 4000,
  filesystem: {
    path: process.env.FILE_SYSTEM_PATH,
    apartments: "/apartments",
  },
  digitalOceanSpaces: {
    endpoint: process.env.DIGITAL_OCEAN_SPACES_ENDPOINT,
    accessKeyId: process.env.DIGITAL_OCEAN_SPACES_ACCESS_KEY_ID,
    secretAccessKey: process.env.DIGITAL_OCEAN_SPACES_SECRET_ACCESS_KEY,
    bucket: process.env.DIGITAL_OCEAN_SPACES_BUCKET,
  },
  paging: {
    defaultSize: 10,
    defaultSkip: 0,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expiresIn: "24h",
  },
  stripe: {
    secret: process.env.STRIPE_SECRET,
  },
  mode: process.env.MODE,
  instance: process.env.INSTANCE,
  domain: process.env.DOMAIN,
}
