const app = require("./app")
const http = require("http").Server(app)
const config = require("../config")
const logger = require("core/logger")

http.listen(config.port, () => {
  logger.info({ message: `Backend listening on port ${config.port}` })
})

module.exports = http
